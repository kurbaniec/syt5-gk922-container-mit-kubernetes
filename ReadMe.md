# SYT GK922 SysInt/Systemmanagement "*Container mit Kubernetes (light)*"

## Kacper Urbaniec | 5AHIT | 07.11.2019

## Aufgabenstellung

### Voraussetzungen

- betriebsbereites Linux (bevorzugt [*Ubuntu 18.04 LTS*](http://cdimage.ubuntu.com/releases/18.04.3/release/ubuntu-18.04.3-server-amd64.iso) oder *Debian Stable*) mit funktionierender Netzwerkverbindung und SSH Zugang (grafische Oberfläche wird *NICHT* benötigt), bevorzugt als virtuelle Maschine (=> Snapshot zu Übungsbeginn nicht vergessen)

### Grundlagen

[What is Kubernetes](https://kubernetes.io/docs/concepts/overview/what-is-kubernetes/)

### Aufgaben

- Durchführen und Dokumentieren der Interactive Tutorials von Kubernetes (ca. 50min):
  - [Create a Cluster](https://kubernetes.io/docs/tutorials/kubernetes-basics/create-cluster/cluster-intro/)
  - [Depoly an App](https://kubernetes.io/docs/tutorials/kubernetes-basics/deploy-app/deploy-intro/)
  - [Explore your App](https://kubernetes.io/docs/tutorials/kubernetes-basics/explore/explore-intro/)
  - [Expose Your App Publicly](https://kubernetes.io/docs/tutorials/kubernetes-basics/expose/expose-intro/)
  - [Scale Your App](https://kubernetes.io/docs/tutorials/kubernetes-basics/scale/scale-intro/)

### Installation und Basis-Konfiguration

### Aufgaben

- Installieren von K3s (ca. 5min)
  - [K3s: Quick-Start Guide](https://rancher.com/docs/k3s/latest/en/quick-start/)
  - Achtung: lokale IP-Adresse muss namentlich auflösbar sein (=> `/etc/hosts` Eintrag)
  - Anmerkung: `curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--no-deploy servicelb" sh -` (damit der LoadBalancer später funktioniert)
- Deploy und Expose mit K3s betreiben (Anleitungen siehe oben) (ca. 30min)
  - Dokumentiere Befehle und Ausgaben, Teste das Ergebnis!

### Ausführliches Beispiel

### Aufgaben

- Um Persistent Volumes mit K3s zu verwenden muss HostPath persistent local storage konfiguriert werden:
  - `kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml`
- Verwendung eines Load-Balancers als Netzwerkzugang
  - `kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.8.1/manifests/metallb.yaml`
  - ConfigMap aus [k3s: Kubernetes Dashboard + load balancer](https://mindmelt.nl/mindmelt.nl/2019/04/08/k3s-kubernetes-dashboard-load-balancer/) übernehmen und anwenden.
- Example: Deploying WordPress and MySQL with Persistent Volumes
  - Anmerkung: `wordpress.yaml` entsprechend [k3s: Kubernetes Dashboard + load balancer](https://mindmelt.nl/mindmelt.nl/2019/04/08/k3s-kubernetes-dashboard-load-balancer/) modifizieren.
  - Testen mit `curl http://`external-ip`/wp-admin/install.php` (curl folgt dem HTTP redirect nicht automatisch).
- K3s um einen weiteren Node erweitern.
  - Testen mit `kubectl get nodes`.

### Abgabemodalitäten

- Dokumentation der Übungsdurchführung (pdf), Abgabe online
  - Verwendete Befehle und Erklärungen dazu
  - Aufgetretene Probleme, Fehlersuche und Debugging
  - Testen und Testergebnisse
- Demonstration der *Basis-Konfiguration*-Aufgabenstellung (sobald abgeschlossen!)
- Demonstration der *WordPress*-Aufgabenstellung (sobald abgeschlossen!)

## Implementierung

## Aufgaben

### Module 1 - Create a Kubernetes cluster

Der nachfolgende Abschnitt befasst sich mit dem Aufsetzen eines einfachen Kubernetes Clusters. Dazu wird `minikube` verwendet, ein Tool, mit dem Kubernetes lokal einfach ausgeführt werden kann.

Schauen ob `minikube` schon installiert ist:

```
minikube version
```

Falls ja, kann das Cluster gestartet werden:

```
minikube start
```

Zum Interagieren mit Kubernetes kann das Command Line Interface `kubectl` verwendet werden.  Zu testen ob `kubectl` vorhanden ist, einfach folgenden Befehl ausführen:

```
kubectl version
```

Um Informationen über das Kubernetes Cluster zu erhalten wird folgender Befehl verwendet:

```
kubectl cluster-info
```

```
Kubernetes master is running at https://172.17.0.18:8443
KubeDNS is running at https://172.17.0.18:8443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy
```

Um alle Nodes zu erhalten wird folgender Befehl verwendet:

```
kubectl get nodes
```

```
NAME       STATUS   ROLES    AGE     VERSION
minikube   Ready    master   4m25s   v1.15.0
```

### Module 2 - Deploy an app

Um eine Applikation in Kubernetes auszuführen, muss diese als Container Applikation konfiguriert werden. Dazu wird eine Deployment Konfiguration benötigt. Diese enthält Abweisungen für Kubernetes,  wie die Applikation erstellt und aktualisiert werden soll. Der folgende Abschnitt zeigt wie man mit Applikationen in Form eines Deployments arbeiten kann.

Zuerst wird eine simple Applikation zum Testen deployed.  Dabei muss ein Name und Speicherort eines Images angegeben werden. 

```
kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
deployment.apps/kubernetes-bootcamp created
```

Dabei wurde einige Sachen automatisiert durchgeführt:

* Die Suche nach einer geeigneten Node für das Projekt
* Das Scheduling, damit die Applikation auf der Node läuft
* Das Cluster konfiguriert, so dass die Instanz auf eine neue Node übertragen werden kann, wenn nötig

Um eine Liste aller Deployments zu erhalten, kann man folgenden Befehl ausführen:

```
kubectl get deployments
```

```
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   1/1     1            1           5m20s
```

Um direkt in dem privaten Netzwerk von Kubernetes zu arbeiten  um auf die Pods/Instanzen zugreifen zu können, kann man folgenden Befehl verwenden:

```
kubectl proxy
```

Dadurch können alle APIs die durch den Proxy gehosted werden angesprochen werde wie:

```
curl http://localhost:8001/version
{
  "major": "1",
  "minor": "15",
  "gitVersion": "v1.15.0",
  "gitCommit": "e8462b5b5dc2584fdcd18e6bcfe9f1e4d970a529",
  "gitTreeState": "clean",
  "buildDate": "2019-06-19T16:32:14Z",
  "goVersion": "go1.12.5",
  "compiler": "gc",
  "platform": "linux/amd64"
}$
```

Um den Namen eines Pods zu erhalten und in eine Variable zu speichern kann der folgende Befehl verwendet werden:

```
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
$ echo Name of the Pod: $POD_NAME
Name of the Pod: kubernetes-bootcamp-75bccb7d87-cm26q
```

### Module 3 - Explore your app

Der folgende Abschnitt befasst sich mit dem Thema Pods. Pods führen eine Instanz eines Programmes aus, der als Deployment konfiguriert wurde.

Um existierende Pods (Instanz/en in einer Node) zu bekommen, kann der folgende Befehl verwendet werden:

```
kubectl get pods
```

```
NAME                                   READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-5b48cfdcbd-6lp5v   1/1     Running   0          68s
```

Mit dem nächsten Befehl kann der Inhalt, also IP-Adressen, Ports und mehr der Pods aufgelistet werden:

```
kubectl describe pods
```

Um  auf den Pod zuzugreifen  muss ein Proxy verwendet werden. Dann wird der Name des Pods benötigt, um den Output der Applikation mittels API anzuzeigen:

```
kubectl proxy

export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')

curl http://localhost:8001/api/v1/namespaces/default/pods/$POD_NAME/proxy/
```

```
Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-5b48cfdcbd-6lp5v | v=1
```

Alles was eine Applikation normalerweise über die Standardausgabe (STDOUT) ausgeben würde, wird in Kubernetes in ein Log geschrieben.

Auf dieses kann man wie folgt zugreifen, wenn man den Namen des Pods kennt:

```
kubectl logs $POD_NAME
```

```
Kubernetes Bootcamp App Started At: 2019-11-07T13:06:08.155Z | Running On:  kubernetes-bootcamp-5b48cfdcbd-6lp5v

Running On: kubernetes-bootcamp-5b48cfdcbd-6lp5v | Total Requests: 1 | App Uptime: 252.471 seconds | Log Time: 2019-11-07T13:10:20.626Z
Running On: kubernetes-bootcamp-5b48cfdcbd-6lp5v | Total Requests: 2 | App Uptime: 408.191 seconds | Log Time: 2019-11-07T13:12:56.346Z
```

Um Kommandos direkt im Pod auszuführen wird das `exec` Kommando verwendet:

```
kubectl exec $POD_NAME env
```

Dies kann verwendet werden um eine Bash-Session zu starten:

```
kubectl exec -ti $POD_NAME bash
```

### Module 4 - Expose your app publicly

Ein Service in Kubernetes ist die Abstraktion, die es erlaubt Pods zu beenden und replizieren, ohne Auswirkungen auf die Applikation zu haben. Ein Service kann über Labels definiert werden, die mit Pods assoziiert werden. Pods in unterschiedlichen Nodes können das gleiche Label haben um Zusammen einen Service zu bilden.

Um aktuelle Service von Kubernetes einzuholen wird der folgende Befehl verwendet:

```
kubectl get services
```

```
NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1    <none>        443/TCP   16s
```

Als nächstes wir ein neuer Service erstellt, der für externe Anfragen geöffnet werden soll:

```
kubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
```

Um zu schauen welcher Port geöffnet worden ist, kann der folgende Befehl verwendet werden:

```
kubectl describe services/kubernetes-bootcamp
```

Zum Testen wird eine Variable erstellt, die den Port der Node speichert und mit Curl verwendet wird, um zu sehen ob die Applikation darüber wirklich erreichbar ist.

```
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')

curl $(minikube ip):$NODE_PORT
```

```
Hello Kubernetes bootcamp! | Running on: kubernetes-bootcamp-5b48cfdcbd-mmt7q | v=1
```

Mittels dem `describe` Befehl von `kubectl` kann eingesehen werden welches Labels das Pod hat.

Mit folgenden Befehl kann nach Pods mit solchen Labels abgefragt werden, in diesem Fall dem Label `run=kubernetes-bootcamp`:

```
kubectl get pods -l run=kubernetes-bootcamp
```

```
NAME                                   READY   STATUS    RESTARTS   AGE
kubernetes-bootcamp-5b48cfdcbd-mmt7q   1/1     Running   0          11m
```

Das selbe kann auch für Services verwendet werden:

```
kubectl get services -l run=kubernetes-bootcamp
```

```
NAME                  TYPE       CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE
kubernetes-bootcamp   NodePort   10.110.248.121   <none>        8080:31890/TCP   10m
```

Neue Labels können wie folgt einem Pod hinzugefügt werden, wobei zuerst der Name abgespeichert wird:

```
export POD_NAME=$(kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')

kubectl label pod $POD_NAME app=v1
```

Dieses Label sollte jetzt per `kubectl describe pods $POD_NAME` ersichtlich sein.

Um einen Service zu löschen kann der folgende Befehl verwendet werden:

```
kubectl delete service -l run=kubernetes-bootcamp
```

Um zu testen, ob der Service wirklich gelöscht wurde:

```
kubectl get services
```

### Module 5 - Scale up your app

Dieser Abschnitt befasst sich mit dem Thema Skalierung. Wenn die Anfragen für eine Applikation steigen, muss die Applikation hoch skaliert werden um den Nutzerdrang stand zu halten. Kubernetes erlaubt es Applikation hoch zu skalieren, aber auch runter zu skalieren.

Mit folgenden Befehl stufen wir das Deployment auf vier Instanzen hoch:

```
kubectl scale deployments/kubernetes-bootcamp --replicas=4
```

Jetzt sollte diese Änderung per ` kubectl get deployments ` ersichtlich sein:

```
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   4/4     4            0           9s
```

Mit folgenden Befehl kann auf die internen IP-Adressen und mehr Einsicht genommen werden:

```
kubectl get pods -o wide
```

Um zu testen ob das Load-Balancing funktioniert speichert wir den Port ab und führen den Curl Befehl mehrere male aus:

```
export NODE_PORT=$(kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')

curl $(minikube ip):$NODE_PORT
```

Es sollten verschiedene Antworten erhalten werden.

Um die Applikation runter zu skalieren, auf zwei Pods, kann der folgende Befehl verwendet werden:

```
kubectl scale deployments/kubernetes-bootcamp --replicas=2
deployment.extensions/kubernetes-bootcamp scaled
```

Das Testen erfolgt dann so:

```
kubectl get deployments
```

```
NAME                  READY   UP-TO-DATE   AVAILABLE   AGE
kubernetes-bootcamp   2/2     2            2           5m13s
```

## Installation und Basis-Konfiguration

K3s ist eine leichtgewichtige Kubernetes Distribution die weniger Arbeitsspeicher  benötigt und für schwächere, ARM basierte Geräte geeignet ist. 

Die  Installation von K3s erfolgt mit folgenden Befehl:

```
curl -sfL https://get.k3s.io | INSTALL_K3S_EXEC="--no-deploy servicelb" sh -
```

### Deploy an App mittels K3s

Zuerst  wird eine Example-Applikation deployed:

```
sudo kubectl create deployment kubernetes-bootcamp --image=gcr.io/google-samples/kubernetes-bootcamp:v1
```

```
kubectl get deployments
```

![](images/1.PNG)

Um direkt in dem privaten Netzwerk von Kubernetes zu arbeiten, um auf die Pods/Instanzen zugreifen zu können, muss ein Proxy im neuen Tab gestartet werden:

```
kubectl proxy
```

Durch diesen Proxy können wir jetzt auf die Applikation zugreifen:

![](images/2.PNG)

### Expose your App mittels K3s

Um aktuelle Service von Kubernetes einzuholen wird der folgende Befehl verwendet:

```
sudo kubectl get services
```

![](images/3.PNG)

Als nächstes wir ein neuer Service erstellt, der für externe Anfragen geöffnet werden soll:

```
ksudo ubectl expose deployment/kubernetes-bootcamp --type="NodePort" --port 8080
```

Um zu schauen welcher Port geöffnet worden ist, kann der folgende Befehl verwendet werden:

```
sudo kubectl describe services/kubernetes-bootcamp
```

![](images/4.PNG)

Zum Testen wird eine Variable erstellt, die den Port der Node speichert und mit Curl verwendet wird, um zu sehen ob die Applikation darüber wirklich erreichbar ist.

```
export NODE_PORT=$(sudo kubectl get services/kubernetes-bootcamp -o go-template='{{(index .spec.ports 0).nodePort}}')

curl 192.168.84.131:$NODE_PORT
```

![](images/5.PNG)

Mittels dem `describe` Befehl von `kubectl` kann eingesehen werden welches Labels das Pod hat.

Mit folgenden Befehl kann nach Pods mit solchen Labels abgefragt werden, in diesem Fall dem Label `app=kubernetes-bootcamp`:

```
sudo kubectl get pods -l app=kubernetes-bootcamp
```

![](images/6.PNG)

Das selbe kann auch für Services verwendet werden:

```
kubectl get services -l app=kubernetes-bootcamp
```

![](images/7.PNG)

Neue Labels können wie folgt einem Pod hinzugefügt werden, wobei zuerst der Name abgespeichert wird:

```
export POD_NAME=$(sudo kubectl get pods -o go-template --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')

sudo kubectl label pod $POD_NAME app=v1 --overwrite
```

Dieses Label sollte jetzt per `kubectl describe pods $POD_NAME` ersichtlich sein.

![](images/8.PNG)

Um einen Service zu löschen kann der folgende Befehl verwendet werden:

```
sudo kubectl delete service -l app=kubernetes-bootcamp
```

Um zu testen, ob der Service wirklich gelöscht wurde:

```
sudo kubectl get services
```

![](images/9.PNG)

Um das Deployment von `kubernetes-bootcamp` komplett zu stoppen, muss die Anzahl der erlaubten Replikationen auf null gesetzt werden.

```
sudo kubectl scale --replicas=0 deployment/kubernetes-bootcamp
```

## Ausführliches Beispiel - Aufgaben

### Persistent Volumes Konfiguration 

Um *Persistent Volumes* mit K3s zu verwenden muss *HostPath persistent local storage* konfiguriert werden: 

```
sudo kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/master/deploy/local-path-storage.yaml
```

![](images/10.PNG)

### Load-Balancer Konfiguration

Folgende Befehle sind notwendig um einen Load-Balancer in K3s einsetzen zu können.

Zuerst wird folgende Konfiguration angewendet.

```
kubectl apply -f https://raw.githubusercontent.com/google/metallb/v0.8.1/manifests/metallb.yaml
```

Jetzt muss noch zusätzliche Konfiguration händischer erstellt und angewendet werden:

 Neues Config-File `loadbalancerconfig.yaml` erstellen:

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  namespace: metallb-system
  name: config
data:
  config: |
    address-pools:
    - name: pod-ralm
      protocol: layer2
      addresses:
      - 192.168.2.240-192.168.2.250
```

Diese Config einsetzen:

```
sudo kubectl apply -f loadbalancerconfig.yaml
```

### Deploying WordPress and MySQL with Persistent Volumes

Zu aller erst laden wir die benötigten Config-Files herunter:

```
curl https://kubernetes.io/examples/application/wordpress/mysql-deployment.yaml > mysql-deployment.yaml
curl https://kubernetes.io/examples/application/wordpress/wordpress-deployment.yaml > wordpress-deployment.yaml
```

Jetzt muss ein File namens `kustomization.yaml` erstellt werden.

Zur `kustomization.yaml` wird ein Secret Objekt hinzugefügt, dass das MySQL Passwort definiert. Außerdem werden die MySQL und Wordpress Konfigurationen hinzugefügt.

```yaml
secretGenerator:
- name: mysql-pass
  literals:
  - password=1234
resources:
    - mysql-deployment.yaml
    - wordpress-deployment.yaml
```

Jetzt können die ganzen Konfigurationen angewendet werden:

```
sudo kubectl apply -k ./
```

Jetzt wird getestet ob alles ordnungsmäßig importiert worden ist:

1. Zuerst wird geschaut ob das "Secret" Objekt existiert:

   ```
   sudo kubectl get secrets
   ```

   ![](images/11.PNG)

   

2. Schauen ob das Persistent Volume dynamisch zur Verfügung gestellt wurde:

   ```
   sudo kubetcl get pvc
   ```

   ![](images/12.PNG)

   

3. Schauen ob alle Pods laufen

   ```
   kubectl get pods
   ```

   ![](images/13.PNG)

   

4. Als letztens schauen ob der Service läuft:

   ```
   sudo  kubectl get services wordpress
   ```

   ![](images/14.PNG)

Jetzt kann die Erreichbarkeit der Wordpress-Seite über die externe IP getestet werden:

```
curl http://192.168.2.241/wp-admin/install.php
```

![](images/15.PNG)

## K3s um weiteren Node erweitern

Zuerst müssen wir unseren den Master Node konfigurieren. Standardmäßig horcht dieser nur auf `localhost`, er soll aber externe Nodes akzeptieren.

Dazu öffnet man die Konfiguration mit `sudo vim /etc/rancher/k3s/k3s.yaml` und ändert die IP-Adresse beim Tag `server`, die standardmäßig auf `https://127.0.0.1:6444` gesetzt ist.

Als nächstes muss der Token des Masters abgefragt werden. Dieser wird beim Registrieren einer weitern Node benötigt.

Zur Abfrage kann folgender Befehl verwendet werden:

```
sudo cat /var/lib/rancher/k3s/server/node-token
```

Jetzt folgt die Erweiterung um eine Node. Dazu habe ich eine neue virtuelle Maschine erstellt. Achtung, der Hostname der neuen Maschine muss anders sein als der Hostname der Maschine die den Master Node repräsentiert! Falls sie den gleichen Namen trägt, kann man per `/etc/hostname` den Namen ändern. Dann muss die Maschine neu gestartet werden. 

Als nächstes habe ich probiert, wie im Quickstart-Guide angegeben, eine neue Node direkt beim installieren zu erstellen.

```
curl -sfL https://get.k3s.io | K3S_URL=https://master-ip:6443 K3S_TOKEN=your-token sh -
```

Diese Methode hat bei mir leider nicht funktioniert, k3s wird installiert und konfiguriert, aber beim Master wir bei der Abfrage mittels `sudo kubectl get nodes` das Fehlen der neuen Node bestätigt.

Ich fand aber einen Weg die Node manuell zu starten und registrieren: 

```
sudo k3s agent --server https://master-ip:6443 --token your-token 
```

![](images/15_5.PNG)

Jetzt wurde die neue Node beim Master registriert und angezeigt:

![](images/16.PNG)

## Quellen

* [Add Node to K3s | 07.11.2019]( https://blog.alexellis.io/create-a-3-node-k3s-cluster-with-k3sup-digitalocean/ )
* [Add Node to K3s | 11.11.2019]( https://www.danielstechblog.io/running-a-kubernetes-cluster-with-k3s-on-raspbian/ )
* [Add Node to K3s manually |11.11.2019]( https://github.com/rancher/k3s/issues/210 )